package com.example.prototipo_proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RegistroMascotas extends AppCompatActivity {

    Button Volvermenuprincipal;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_mascotas);


        Volvermenuprincipal= (Button) findViewById(R.id.btnVolver2);


        // metodos para ejecutar los botones mediante un click

        Volvermenuprincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent VOLVER= new Intent (RegistroMascotas.this,MenuPrincipal.class);
                startActivity(VOLVER);
            }
        });



    }
}