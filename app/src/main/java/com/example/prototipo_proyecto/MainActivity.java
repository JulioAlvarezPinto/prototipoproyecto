package com.example.prototipo_proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button IrRegistro, IrIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IrRegistro= (Button) findViewById(R.id.btnRegistrar);
        IrIngresar= (Button) findViewById(R.id.btnIngresar);


        // metodos para ejecutar los botones mediante un click
       // IR DEL LOGIN MEDIANTE EL BOTON 'REGISTRAR' A LA ACTIVIDAD 'MAIN ACTIVITY QUE ES EL LOGIN'
        IrRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir= new Intent (MainActivity.this,Registro.class);
                startActivity(ir);
            }
        });


        // IR DEL LOGIN MEDIANTE EL BOTON 'INGRESAR' A LA ACTIVIDAD 'MENU PRINCIPAL'
        IrIngresar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent ir= new Intent (MainActivity.this,MenuPrincipal.class);
            startActivity(ir);
        }
    });

    }
}